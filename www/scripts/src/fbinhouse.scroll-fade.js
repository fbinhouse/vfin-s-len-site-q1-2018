( function( $, _ ){
    'use strict';

    var defaultAnimOptions = {
        duration: 300,
        ease: 'easeOutSine',
        stagger: 0,
        display: 'block',
        begin: function( elements ){
            $( elements ).addClass( 'js-scroll-animated' );
        }
    }
    var setAnimOptions = function( options ){
        var animOptions = options;
        animOptions.duration = options.duration ? options.duration : defaultAnimOptions.duration;
        animOptions.ease = options.ease ? options.ease : defaultAnimOptions.ease;
        animOptions.stagger = options.stagger ? options.stagger : defaultAnimOptions.stagger;
        animOptions.display = options.display ? options.display : defaultAnimOptions.display;
        animOptions.begin = defaultAnimOptions.begin;

        return animOptions;
    }

    $.fn.fadeInScroll = function() {

        // VARIABLES -----------------------------------
        var elements = $( this );
        var minY = 75;

        // SETTINGS ------------------------------------
        var settings = $.extend({
            minDistance: minY * $( window ).height() / 100   //Distance between the browser top scroll and the next element
        });

        // DEFINE ANIMATION ----------------------------
        var animation = {};

        if( arguments.length > 0 ){
            if( arguments.length === 1 ){
                if( typeof arguments[0] === 'object' ){
                    animation.options = setAnimOptions( arguments[0] );
                } else {
                    animation.name = arguments[0];
                    animation.options = defaultAnimOptions;
                }
            } else {
                animation.name = arguments[0];
                animation.options = setAnimOptions( arguments[1] );
            }
        } else {
            animation.name = 'transition.fadeIn';
            animation.options = defaultAnimOptions;
        }

        // MAIN ----------------------------------------
        // Add sFade classes
        $( elements ).each( function(){
            $( this ).css( 'opacity', '0' );
        });

        // Check the position of all the elemnents.
        //fbinhouse.CheckFades();

        // FUNCTIONS ------------------------------------
        fbinhouse.checkFades = function(){

            //Get the top of the browser
            var vWindowScrollTop = $( window ).scrollTop();

            //Test if the window TopScroll reachs the element TopScroll minus the minimun distance
            if( animation.options.stagger > 0 ){
                if( !$( elements ).first().hasClass( 'js-scroll-animated' ) && vWindowScrollTop + parseInt( settings.minDistance, 10 ) >= $( elements ).first().offset().top ){
                    $( elements )
                        .velocity( 'stop' )
                        .velocity( animation.name === undefined ? animation.options : animation.name, animation.options );
                }
            } else {
                $( elements ).each( function(){
                    if( !$( this ).hasClass( 'js-scroll-animated' ) && vWindowScrollTop + parseInt( settings.minDistance, 10 ) >= $( this ).offset().top ){
                        $( this )
                            .velocity( 'stop' )
                            .velocity( animation.name === undefined ? animation.options : animation.name, animation.options );
                    }
                });
            }
        }

        // EVENTS ----------------------------- -------
        $( window ).on( 'scroll', _.throttle( fbinhouse.checkFades, 100 ));
    };

})( jQuery, _ );
