fbinhouse.init = function($) {
    'use strict';

    fbinhouse.detectIe($);

    $('html').addClass('loaded');

    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        var linkSrc = $('.input-button');

        if (/android/i.test(userAgent)) {
            linkSrc.attr('href', 'market://details?id=se.volvofinans.carpay');
        } else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            linkSrc.attr('href', 'https://geo.itunes.apple.com/us/app/carpay/id1063224472?mt=8');
        } else {
            linkSrc.attr('href', 'https://geo.itunes.apple.com/us/app/carpay/id1063224472?mt=8');
        }
    }

    fbinhouse.initLargeImages();
    fbinhouse.initGalleryGrid('.gallery-grid');
    fbinhouse.callbacks.initCallbacks();

    $('.fbi-headerblock').imagesLoaded().done(function() {
        fbinhouse.preloader.fadeOut();
    });

    $('#fbi-content').imagesLoaded().done(function(elem) {
        fbinhouse.resizeLargeImages();
        getMobileOperatingSystem();
    });

    $('.scroll-down-icon').on('click', function() {
        $('html, body').animate({
            scrollTop: $('.intro-ingress').offset().top
        }, 650);
    });


};
