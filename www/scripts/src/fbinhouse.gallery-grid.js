( function( $ ){
    'use strict';

    function GalleryGrid( selector ){
        var images = $( selector ).find( 'img' );
        var $gridSliderContainer = $( '<div>', {
            class: 'gallery-slider active'
        });
        var gridSlider;

        var initGrid = function(){
            var i = 0;
            var $grid = $( '<div>', {
                class: 'gallery-grid-wrapper'
            }).append(
            );
            var $owlGrid = $( '<div>', {
                class: 'gallery-grid-inner'
            }).append( [
                $( '<div class="gallery-grid-col" data-col-id="1">' ),
                $( '<div class="gallery-grid-col" data-col-id="2">' ),
                $( '<div class="gallery-grid-col" data-col-id="3">' ),
                $( '<div class="gallery-grid-col" data-col-id="4">' )
            ] )
            var cols = $owlGrid.find( '.gallery-grid-col' );
            var getItemHolder = function( tall ){
                var size = tall ? 'tall' : 'small'
                var $itemHolder = $( '<div>', {
                    class: 'gallery-item-holder ' + size
                });

                return $itemHolder;
            }
            var itemHolder;
            var img;
            var imgSrc;
            var colIndex;
            var item;
            var image;
            var fill;

            $grid.append( $owlGrid );
            $( selector ).append( $grid );

            for( i; i < images.length; i = i + 1 ){
                img = $( images[i] );
                colIndex = img.data( 'grid-col' ) - 1;
                imgSrc = img.attr( 'src' ).split( '/' );
                imgSrc = fbinhouse.baseUrl + 'img/srcsets/768/' + imgSrc[ imgSrc.length - 1 ];
                image = $( '<div>', { class: 'gallery-item-thumb' }).css( {
                    'background-image': 'url(' + imgSrc + ')',
                    'background-position': img.data( 'alignment' )
                });
                fill = images[i].hasAttribute( 'data-fill' ) ? 'fill' : '';
                item = $( '<a>', {
                    class: 'gallery-item js-gallery-item ' + fill,
                    'data-id': i + 1
                }).append( image );
                itemHolder = getItemHolder( images[i].hasAttribute( 'data-grid-tall' ) ? true : false );
                itemHolder.append( item );
                $( cols[ colIndex ] ).append( itemHolder );
            }

            $( 'body' ).on( 'click', '.js-gallery-item', function( event ){
                var $thumb = $( event.currentTarget );
                var id = $thumb.data( 'id' ) - 1;

                gridSlider.trigger( 'to.owl.carousel', [ id, 1 ] );

                gridSlider
                    .velocity( 'transition.fadeIn', {
                        duration: 400,
                        delay: 40,
                        complete: function(){
                            fbinhouse.resizeLargeImages();
                        }
                    })
                    .addClass( 'active' );

                event.preventDefault();
            });

            $owlGrid.owlCarousel({
                loop: true,
                margin: 0,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: false,
                        dots: true
                    },
                    500: {
                        items: 2,
                        nav: false,
                        dots: true
                    },
                    900: {
                        items:4,
                        nav:false,
                        loop:false,
                        dots: false
                    }
                }
            })

            $( selector )
                .velocity( 'transition.fadeIn', {
                    duration: 100,
                    delay: 100
                });
        }

        var initGridSlider = function(){
            var sliderItems = $( selector ).children();
            var $closeGridSlider = $( '<a>', {
                class: 'close-grid-slider js-close-grid-slider'
            });

            sliderItems.wrapAll( $gridSliderContainer );
            gridSlider = $( selector ).find( '.gallery-slider' );

            $closeGridSlider.on( 'click', function(){
                gridSlider.velocity( 'transition.fadeOut', {
                    duration: 400,
                    display: 'block',
                    complete: function(){
                        $( '.gallery-slider' ).removeClass( 'active' );
                    }
                });

                event.preventDefault();
            });
            gridSlider
                .owlCarousel({
                    loop: true,
                    margin: 0,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true,
                            dots: true
                        }
                    }
                })
                .velocity( 'transition.fadeOut', {
                    duration: 1,
                    display: 'block',
                    complete: function(){
                        $( '.gallery-slider' ).removeClass( 'active' );
                    }
                })
                .append( $closeGridSlider );

            initGrid();
        }

        $( '#fbi-content' ).imagesLoaded()
        .done( function(){
            initGridSlider();
        })
    }

    fbinhouse.initGalleryGrid = function( selector ){
        if( typeof fbinhouse.galleryGrids === 'undefined' ){
            fbinhouse.galleryGrids = [];
        }

        $( selector )
            .addClass( 'fbi-gallery-grid' )
            .each( function(){
                fbinhouse.galleryGrids.push( new GalleryGrid( selector ));
            });
    };

})( jQuery );
